#!/usr/bin/env bash

set -e
set -x

rm -rf out
mkdir -p out

python3 -m unittest parsers.test_args

echo "Plotting Crysol output"

bin/dplot "out/crysol_m.png" --other "in/crysol.int" \
    --headers "Q,Intensity,Scattering (in vacuo),Scattering (excluded volume),Convex border layer" \
    --skip 1 -x "Q" --log
bin/dplot "out/crysol.png" --crysol "in/crysol.int"

echo "Plotting Debyer output"

bin/dplot "out/debyer_m.png" --other "in/debyer.dat" --skip 2 --headers "Q,Intensity" -x "Q" --log
bin/dplot "out/debyer.png" --debyer "in/debyer.dat"

echo "Plotting psaxs output"

bin/dplot "out/psaxs_m.png" --other "in/psaxs.tsv" --headers "Q,Intensity" -x "Q" --log
bin/dplot "out/psaxs.png" --psaxs "in/psaxs.tsv"

echo "Plotting psaxs, Crysol and Debyer output on one plot"

bin/dplot "out/combined_m.png" --other "in/psaxs.tsv" --headers "Q,Intensity" -x "Q" --crysol "in/crysol.int" --debyer "in/debyer.dat"
bin/dplot "out/combined.png" --psaxs "in/psaxs.tsv" --crysol "in/crysol.int" --debyer "in/debyer.dat"

plots=(combined crysol debyer psaxs)

for plot in "${plots[@]}"; do
  [[ $(compare -metric rmse "out/${plot}_m.png" "out.reference/${plot}.png" null: 2>&1) = "0 (0)" ]]
  [[ $(compare -metric rmse "out/${plot}.png" "out.reference/${plot}.png" null: 2>&1) = "0 (0)" ]]
done
