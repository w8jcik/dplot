# Clone

```shell
git clone git@gitlab.com:cbjh/plotting/dplot.git
```

# Dependencies

- `pandas`
- `seaborn`
- `matplotlib`
- Python 3.6 - 3.9

With _pyenv_ or _conda_

```
pip3 install -r requirements.txt
```

# Install

```bash
cd dplot
ln -s `pwd`/bin/dplot ~/.local/bin
```

`~/.bashrc`

```diff
+ PATH=$PATH:$HOME/.local/bin
```

(changing `.bashrc` requires reopening of the terminal or running `source ~/.bashrc`)

# Examples

## `crysol` output

```bash
dplot "out/crysol.png" --other "in/crysol.int" \
    --headers "Q,Intensity,Scattering (in vacuo),Scattering (excluded volume),Convex border layer" \
    --skip 1 -x "Q" --log
```

or

```bash
dplot "out/crysol.png" --crysol "in/crysol.int"
```

![crysol](out.reference/crysol.png)

## `debyer` output

```bash
dplot "out/debyer.png" --other "in/debyer.dat" --skip 2 --headers "Q,Intensity" -x "Q" --log
```

or

```bash
dplot "out/debyer.png" --debyer "in/debyer.dat"
```

![debyer](out.reference/debyer.png)

## `psaxs` output

```bash
dplot "out/psaxs.png" --other "in/psaxs.tsv" --headers "Q,Intensity" -x "Q" --log
```

or

```bash
dplot "out/psaxs.png" --psaxs "in/psaxs.tsv"
```

![psaxs](out.reference/psaxs.png)

## Multiple sources

```bash
./dplot.py "out/combined.png" --psaxs "in/psaxs.tsv" --crysol "in/crysol.int" --debyer "in/debyer.dat"
```

Mixing manual plots with presets works as well

```bash
./dplot.py "out/combined.png" --other "in/psaxs.tsv" --headers "Q,Intensity" -x "Q" --crysol "in/crysol.int" --debyer "in/debyer.dat"
```

![combined](out.reference/combined.png)

## Window

Provide `-` instead of a file name to open a window

```bash
dplot - --psaxs "in/psaxs.tsv"
```

![window](out.reference/window.png)

# Tests

```bash
sudo apt install imagemagick
```

```bash
./test.sh
```

# Possible options

Unfortunately `--help` won't work.

Use `--verbose` to see possible switches.
